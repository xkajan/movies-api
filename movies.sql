create table if not exists movie ( 
    id integer not null primary key,
    title text not null,
    description text,
    release_year integer not null
);
