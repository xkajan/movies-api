# Movies API

## Dockerizing Solution

- First install and run Docker on your system.
- To build and run the solution use command `docker-compose up -d`

## Running Solution whithout Docker

- Install Python (recommended version at least 3.7)
- *Optional*: Create a virtual enviroment using following commands (macOS/Linux):
  
``` (shell)
python3 -m venv .venv
. .venv/bin/activate
```

or (Windows):

``` (PowerShell)
python -m venv .venv
.venv\Scripts\activate
```

- Install Flask using command `pip install Flask`
- Run the app using command `python -m flask run`
