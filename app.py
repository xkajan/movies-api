from flask import Flask, request, abort

import sqlite3
import json

def initdb( conn:sqlite3.Connection, sql_file: str ) -> None:
    with open(sql_file) as file:
        conn.executescript(file.read())


def get_connection() -> sqlite3.Connection:
    con = sqlite3.connect("movies.db")
    return con


def movies_sql2dict(row):
    return {"id": row[0], "title": row[1], "description": row[2], "release_year": row[3]}


app = Flask(__name__)
initdb(get_connection(), "movies.sql")


@app.route("/")
def main():
    return "Hello, World!"


@app.route("/movies", methods = ["GET", "POST"])
def movies_API():
    conn = get_connection()
    
    if request.method == "POST":
        title = request.form["title"]
        description = request.form.get("description")
        release_year = request.form["release_year"]
        id = conn.execute("INSERT INTO movie(title, description, release_year) VALUES (?, ?, ?)",
                      (title, description, release_year)).lastrowid
        
        row = [id, title, description, release_year]
        result = json.dumps(movies_sql2dict(row))
        conn.commit()

    else:
        movies = conn.execute("SELECT * FROM movie").fetchall()
        result = json.dumps(list(map(lambda row: movies_sql2dict(row), movies))) 

    conn.close()

    return result


@app.route("/movies/<int:id>", methods = ["GET", "PUT"])
def movie_specific_API(id: int):
    conn = get_connection()

    movie = conn.execute("SELECT * FROM movie WHERE id = ?", (id,)).fetchone()
    if movie is  None:
        return abort(404)
    
    if request.method == "PUT":
        title = request.form["title"]
        release_year = request.form["release_year"]
        description = request.form.get("description")
        conn.execute("UPDATE movie SET title = ?, description = ?, release_year = ? WHERE id = ?",
                      (title, description, release_year, id))
        movie = [id, title, description, release_year]
        conn.commit()
    
    conn.close()

    return json.dumps(movies_sql2dict(movie))


if __name__ == "__main__":
    app.run()
